package io.phasetwo.service.model.jpa.entity;

public enum Status {
    NEW,
    APPROVED,
    ACTIVE,
    INACTIVE,
    EXPIRED,
    DELETED,
    SUCCESS,
    FAILED,
    PENDING,
    SENT,
    RECEIVED,
    READ,
    ONLINE,
    OFFLINE,
    CANCELED,
    ERROR,
    WAITING,
    CANCELLED,
    CONFIRM_ERROR,
    CONFIRM_CANCELLED,
    UNKNOWN,
    DELETED_ON_SYNC,
    SIGNED,
    USER_CREATED,
    USER_NOT_CREATED,
    FINISHED,
    NOAPPROVED
}
